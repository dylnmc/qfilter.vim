qfilter.vim
===========

*A filter for quickfix and loclist in vim*

### Overview

For help understanding what quickfix is, refer to `:help quickfix` in vim. It
will suffice to say that the quickfix list is very useful if you are familiar
with the `:make` and `:grep` commands. As a small example, if you run `:grep!
"foo" someFile`, then you can use `:copen` to open the quickfix list and see the
results. Pressing `<Return>` on any entry will jump to that file on the
particular line and column in a separate window.

This plugin uses `ftplugin` to create a few commands and mappings for the
quickfix and location lists--both commonly referred to as a qf list--to help
filter or narrow down results. This plugin exposes the buffer-local `:Keep` and
`:Reject` commands that will either keep or discard lines by matching patterns
in the metadata inside the qf list as well as operators, motions, and pending
operators that allow the user to further filter the qf lists. See
[Operators and Motions](#operators-and-motions) for more details.

### Commands

| Command  | Description |
| -------  | ----------- |
| `:Keep \<bar\>`     | Keep all lines that contain `bar`. This is a regex match, and it searches the *text metadata* in the qf list. Note the `/\<` and `/\>` which means "bar" must not be embedded in a larger word. |
| `:Reject bar`       | Same as `:Keep /bar/` but remove all lines that contain `bar`. Also, note that since there is no `/\<` or `/\>`, "bar" can be embedded in a larger word. |
| `:KeepFile a.txt`   | Keep all lines that contain the partial match for a file `a.txt`. This is not a regex match (so `/.` will match a literal `'.'`), and it searches the *file metadata* in the qf list. Note: **it will match a _substring_**. |
| `:RejectFile a.txt` | Same as above but remove all lines that partially match `a.txt`. |
| `:KeepAll foo`      | Same as `:Keep /foo/` but this will also match (with vim regex) files. I.e., both `foobar.txt` in the file metadata and `function s:foo()` in the text metadata will be kept. |
| `:RejectAll foo`    | Same as `:KeepAll /foo/` but instead of keeping `foo`, reject it. |

The commands above follow the format `:Command query`. The query can optionally
be wrapped in any non-alphanumeric character to account for whitespace.
Therefore, `:Keep foo`, `:Keep /foo/`, and `:Keep &foo&` are all identical.
However, `:Keep / foo/` is different than `:Keep  foo`. This means that you will
have to be careful about leading and trailing characters if you decide not to
wrap the query in a non-alphanumeric character.

### Operators and Motions

#### Delete

This plugin extends `d` to be the delete operator. When in normal mode in a qf
list, use `dd`, `d{motion}` (e.g., `d2j`), or `d` after visually selecting with
`V` to remove the specified lines.

#### File Jump

In normal, visual, and operator-pending modes, there are also two motions that
allow you to jump between groups of files. In normal mode, `J` and `K` jump to
the **top** of the *next* or *previous* (resp.) group of files. This can be
helpful to quickly inspect various groups of files. In visual and
operator-pending mode, the behavior is similar but slightly modified so that it
behaves more reasonably: the cursor will jump to the bottom or top (resp.) of
the *current* group of files.

Therefore, pressing `J` in normal mode will jump to the *next* group of files.
However, using `J` in conjunction with the `d` operator (like `dJ`) will delete
only the line under the cursor and all lines under it that have the same
filename in the metadata. Similarly, `VJ` will select only lines from the
current line down to the last line in the file group under the cursor.

This doesn't accept a count, and repeating the motion in visual mode will not
change the cursor.

#### Inner File

This is an extension of the *File Jump* motions/pending-operators. If `if` is
used as a pending operator or in visual mode, then it will select the group of
files under the cursor. For example, `VKoJ` is equivalent to `Vif` (hence why it
is an "extension" of *File Jump*). Similarly, `dif` is equivalent to `VKoJd`.

### TODO

- Allow for `:KeepFile` and `:RejectFile` to match only whole files by
  potentially adding `/^` and `/$` to the beginning/end (resp.) of the pattern
  but only if `/[^[:alnum:]]` surrounding the pattern (still stays a non-regex
  search, though)
- Allow `J`/`K` to move to different groups in visual mode? Maybe this can be
  achieved with an `s:` variable that gets set, but seems questionable.
